FROM python:3.9-alpine3.13

# python-gitlab
RUN pip3 install python-gitlab==1.9

# Git and SSH
RUN apk add git openssh less

# Scripts
WORKDIR /scripts
COPY . /scripts/
RUN /scripts/bin/install
ENV PATH="/root/.local/bin:$PATH"

WORKDIR /workdir
