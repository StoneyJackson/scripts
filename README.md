# Scripts

My personal scripts for various tasks. Mostly GitLab, Git, and Brightspace (an LMS).


## 1. Simplest Usage

> :warning: **INSECURE:** This first example is simple, but it is not secure. Use it to understand how things work, but then move on.

Assuming all the configuration files needed by the scripts are in their default location in your home directory (`~/.python-gitlab`, `~/.gitconfig`, and `~/.ssh`), you could mount your entire home directory into the container and start using the scripts immediately.

```bash
docker run -it --rm \
    -v "$HOME:/root" \
    registry.gitlab.com/stoneyjackson/scripts \
    COMMAND
```

Where `COMMAND` is any script in the the `src/` hierarchy in this project.


## 2. Volumes

> :warning: **INSECURE:** Getting better, but keep reading.

You probably shouldn't mount your entire home directory into the container. Instead you should only mount the configuration files that it needs.

Mounting configuration files explicitly also you to specify which configuration files to use.

* Mount your `.gitconfig` file to `/root/.gitconfig`.
* Mount your `.python-gitlab.cfg` file to `/root/.python-gitlab.cfg`. See [python-gitlab's documentation](https://python-gitlab.readthedocs.io/en/stable/index.html) for details. See `examples/.python-gitlab.cfg` in the root of this project for an example.
* Mount the `ssh` named volume to `/ssh`. Use this if you are using a using an SSH agent to manage your identities and git is authenticating using SSH.
* Mount your `.ssh/` directory to `/root/.ssh`. This should be avoided as it gives the container access to secrets that it does not need. Instead, consider using an SSH agent.


## 3. Running using an SSH Agent

> :information_source: This is the most secure method to the best of our knowledge and ability. This is not a warrantee or guarantee. Please do your homework and let us know if we can do better.

This is the best approach. First, you will only entrust your keys to a trusted SSH agent container. And you will only entrust it with just the keys/identities you want to use. Second, you will not need to retype your passphrase for each operation. Just the first time you load a key into the agent.

Here we describe how to setup an SSH agent using the [whilp/ssh-agent](https://github.com/whilp/ssh-agent). It is basically a Dockerization of OpenSSH's ssh-agent. I encourage you to read its documentation and source code (which is small) and convince yourself that it can be trusted rather than take my word for it. It's important that you trust it before using it.

First start a long-running ssh-agent container.

```bash
docker run -d -v ssh:/ssh --name=ssh-agent whilp/ssh-agent@sha256:d6745a55768cbdd32eb9e53ea729d1b8624ad3948846ce945d58d58ebf2dcd33
```

Notice we are using an exact image of whilp/ssh-agent rather than whilp/ssh-agent:latest. Otherwise the author could point whilp/ssh-agent:latest to a different image that we have not yet inspected.

Run the following for each identity/key you want to load into the agent.

```bash
docker run --rm -v ssh:/ssh -v $HOME:$HOME -it whilp/ssh-agent@sha256:d6745a55768cbdd32eb9e53ea729d1b8624ad3948846ce945d58d58ebf2dcd33 ssh-add $HOME/.ssh/id_rsa
```

As long as the ssh-agent container continues to run, you won't need to do the above again.

Now you can run scripts as follows.

```bash
docker run -it --rm \
    -v ssh:/ssh \
    -e SSH_AUTH_SOCK=/ssh/auth/sock \
    -v "$HOME/.gitconfig:/root/.gitconfig:ro" \
    -v "$HOME/.python-gitlab.cfg:/root/.python-gitlab.cfg:ro" \
    registry.gitlab.com/stoneyjackson/scripts \
    COMMAND
```

## 4. Example installation

`examples/docker-compose.yaml` contains an example of a Docker Compose file that can be used to configure a system with an SSH agent and the scripts container. You could copy this and edit it for the exact locations of your configuration files.

```bash
mkdir -p ~/.config/scripts/
cp example/docker-compose.yaml ~/.config/scripts
```

Edit this docker-compose.yaml file if your config files are in non-default locations.

Add some aliases to your shells boot scripts (e.g., .zshrc)

```bash
SCRIPTS_DOCKER_COMPOSE="$HOME/.config/scripts/docker-compose.yaml"
export SCRIPTS_DOCKER_COMPOSE
alias scripts-ssh-agent="docker-compose -f $SCRIPTS_DOCKER_COMPOSE up -d ssh-agent"
alias scripts-ssh-add="docker-compose -f $SCRIPTS_DOCKER_COMPOSE run --rm ssh-add"
alias scripts="docker-compose -f $SCRIPTS_DOCKER_COMPOSE run --rm scripts"
```

Now you can use the system like so.

```bash
scripts-ssh-agent
scripts-ssh-add $HOME/.ssh/id_rsa
scripts SOME-COMMAND
```
